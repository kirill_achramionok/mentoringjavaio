package com.company.random;

import java.io.RandomAccessFile;
import java.io.Serializable;

/**
 * @author kirill.akhramenok
 */
public class Main implements Serializable {
    public static void main(String[] args) throws Exception {
        writeWithRandomAccessFile();
        readWithRandomAccessFile();
        writeToEndWithRandomAccessFile();
        writeToMiddleWithRandomAccessFile();
        int length = 0;

        System.out.println(length + 1 + 3);

    }

    public static void readWithRandomAccessFile() throws Exception {
        RandomAccessFile randomAccessFile = new RandomAccessFile("resources/randomAccessFile.txt", "r");
        System.out.println(randomAccessFile.length());
    }

    public static void writeWithRandomAccessFile() throws Exception {
        try (RandomAccessFile randomAccessFile = new RandomAccessFile("resources/randomAccessFile.txt", "rw")) {
            randomAccessFile.writeBytes("0123456789");
        }
    }

    public static void writeToEndWithRandomAccessFile() throws Exception {
        try (RandomAccessFile randomAccessFile = new RandomAccessFile("resources/randomAccessFile.txt", "rw")) {
            randomAccessFile.seek(10);
            randomAccessFile.getFilePointer();
            randomAccessFile.writeBytes("abcdefg");
        }
    }

    public static void writeToMiddleWithRandomAccessFile() throws Exception {
        try (RandomAccessFile randomAccessFile = new RandomAccessFile("resources/randomAccessFile.txt", "rw")) {
            randomAccessFile.seek(10);
            randomAccessFile.writeBytes(" MIDDLE ");
        }
    }
}
