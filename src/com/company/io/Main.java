package com.company.io;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) throws Exception {
//        writePerson();
//        readPerson();
        writeCollection();
        readCollection();
    }

    public static void readByByte(InputStream inputStream) throws Exception {
        int oneByte = -1;
        while ((oneByte = inputStream.read()) != -1) {
            System.out.print((char) oneByte);
        }
        System.out.println("\n end");
    }

    public static void writeByByte(OutputStream outputStream) throws Exception {
        for (int i = 0; i < 128; i++) {
            outputStream.write(i);
        }
    }

    public static void fileInputStream(File file) {

        try (FileInputStream fileInputStream = new FileInputStream(file)) {
            System.out.println("____________________START____________________");
            for (byte c : fileInputStream.readAllBytes()) {
                System.out.print((char) c);
            }
            System.out.println("\n_____________________END_____________________");
            System.out.println(file.getAbsolutePath());
            System.out.println(file.getCanonicalPath());
            System.out.println(file.getPath());
        } catch (IOException ex) {
            System.out.println(ex);
        }
    }

    public static void fileOutputStream(File file) {
        String string = "File Output Stream in action!";
        try (FileOutputStream fileOutputStream = new FileOutputStream(file)) {
            fileOutputStream.write(string.getBytes());
        } catch (IOException ex) {
            System.out.println(ex);
        }
    }

    public static void writePerson() throws Exception {
        Person person = new Person("Alexey", 18);
        try (ObjectOutputStream objectOutputStream = new ObjectOutputStream(new FileOutputStream("person.txt"))) {
            objectOutputStream.writeObject(person);
        }
    }

    public static void readPerson() throws Exception {
        Person person = null;
        try (ObjectInputStream objectInputStream = new ObjectInputStream(new FileInputStream("person.txt"))) {
            person = (Person) objectInputStream.readObject();
            System.out.println(person.getAge());
            System.out.println(person.getName());
        }
    }

    public static void writeCollection() throws Exception {
        Person person = new Person("Alexey", 18);
        Person person1 = new Person("Dima", 18);
        List<Person> personList = new ArrayList<>();
        personList.add(person);
        personList.add(person1);
        try (ObjectOutputStream objectOutputStream = new ObjectOutputStream(new FileOutputStream("person.txt"))) {
            objectOutputStream.writeObject(personList);
        }
    }

    public static void readCollection() throws Exception {
        List<Person> personList = null;
        try (ObjectInputStream objectInputStream = new ObjectInputStream(new FileInputStream("person.txt"))) {
            personList = (List<Person>) objectInputStream.readObject();
        }
        personList.forEach(p -> {
            System.out.println(p.getName());
            System.out.println(p.getAge());
        });
    }

}
