package com.company.writer.reader.buffered;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.nio.file.Files;
import java.util.Random;
import java.util.Scanner;

/**
 * @author kirill.akhramenok
 */
public class Main {
    public static void main(String[] args) throws Exception {
        Random random = new Random();
        StringBuilder builder = new StringBuilder(10000);
        String content;
        for (int i = 0; i < 1000000; i++) {
            builder.append(random.nextInt(256));
        }
        content = builder.toString();
//        writeWithSmallBuffer(content);
//        writeWithLargeBuffer(content);
        readWithSmallBuffer();
        readWithLargeBuffer();
    }

    public static void writeWithSmallBuffer(String content) throws Exception {

        try (BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter("resources/smallBuffer.txt"), 1)) {
            long start = System.currentTimeMillis();
            bufferedWriter.write(content);
            long finish = System.currentTimeMillis();

            System.out.println("Small buffer Time: " + (finish - start));
        }
    }

    public static void writeWithLargeBuffer(String content) throws Exception {
        try (BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter("resources/largeBuffer.txt"), 1000000)) {
            long start = System.currentTimeMillis();
            bufferedWriter.write(content);
            long finish = System.currentTimeMillis();
            System.out.println("Large buffer Time: " + (finish - start));
        }
    }

    public static void readWithSmallBuffer() throws Exception {
        StringBuilder builder = new StringBuilder();
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader("resources/smallBuffer.txt"), 1)) {
            int buf = -1;
            long start = System.currentTimeMillis();
            while ((buf = bufferedReader.read()) != -1) {
                builder.append(buf);
            }
            long finish = System.currentTimeMillis();
            System.out.println("Small buffer read Time: " + (finish - start));
        }
    }
    public static void readWithLargeBuffer() throws Exception {
        StringBuilder builder = new StringBuilder();
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader("resources/largeBuffer.txt"), 10000)) {
            int buf = -1;
            long start = System.currentTimeMillis();
            while ((buf = bufferedReader.read()) != -1) {
                builder.append(buf);
            }
            long finish = System.currentTimeMillis();
            System.out.println("Large buffer read Time: " + (finish - start));
        }
    }

    public static void scannerExample(){
        Files
    }
}
