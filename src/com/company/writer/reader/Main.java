package com.company.writer.reader;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;

/**
 * @author kirill.akhramenok
 */
public class Main {
    public static void main(String[] args) throws Exception {
        withoutAppend();
        withAppend();
        withoutAppend();
        withAppend();
    }

    public static void withoutAppend() throws Exception {
        File file = new File("resources/withoutAppend.txt");
        FileWriter fileWriter = new FileWriter(file, false);
        String content = "Start";
        BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
        bufferedWriter.write(content);
        bufferedWriter.close();
        fileWriter.close();
    }

    public static void withAppend() throws Exception {
        File file = new File("resources/withAppend.txt");
        FileWriter fileWriter = new FileWriter(file, true);

        String content = "Start";
        BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
        bufferedWriter.write(content);
        bufferedWriter.close();
        fileWriter.close();
    }
}
